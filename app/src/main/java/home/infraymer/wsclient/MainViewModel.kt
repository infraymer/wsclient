package home.infraymer.wsclient

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.chibatching.kotpref.livedata.asLiveData
import com.neovisionaries.ws.client.*
import home.infraymer.wsclient.entity.Message
import home.infraymer.wsclient.system.CustomWebSocketListener
import home.infraymer.wsclient.system.liveData.SingleLiveEvent
import java.util.*

class MainViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        const val STATE_CREATED = 0
        const val STATE_CONNECTING = 1
        const val STATE_OPEN = 2
        const val STATE_CLOSING = 3
        const val STATE_CLOSED = 4
    }

    private val context = getApplication<App>()

    private var webSocket: WebSocket? = null
    private val webSocketListener = object : WebSocketAdapter() {

        override fun onConnected(websocket: WebSocket?, headers: MutableMap<String, MutableList<String>>?) {

        }

        override fun onDisconnected(websocket: WebSocket?, serverCloseFrame: WebSocketFrame?, clientCloseFrame: WebSocketFrame?, closedByServer: Boolean) {

        }

        override fun handleCallbackError(websocket: WebSocket, cause: Throwable) {
            val message = newMessage("Server", "Error: ${cause.message}")
            val msgs = messages.value!!.apply { add(message) }
            messages.postValue(msgs)
        }

        override fun onTextMessage(websocket: WebSocket?, text: String) {
            val message = newMessage("Server", text)
            val msgs = messages.value!!.apply { add(message) }
            messages.postValue(msgs)
        }

        override fun onStateChanged(websocket: WebSocket, newState: WebSocketState) {
            val message = newMessage("System", newState.name)
            val msgs = messages.value!!.apply { add(message) }
            messages.postValue(msgs)
            when (newState) {
                WebSocketState.CREATED -> state.postValue(STATE_CREATED)
                WebSocketState.CONNECTING -> state.postValue(STATE_CONNECTING)
                WebSocketState.OPEN -> state.postValue(STATE_OPEN)
                WebSocketState.CLOSING -> state.postValue(STATE_CLOSING)
                WebSocketState.CLOSED -> state.postValue(STATE_CLOSED)
            }
        }
    }
    private val customWebSocketListener = CustomWebSocketListener(
        onStateChangedMain = { ws, newState ->
            val message = newMessage("System", newState.name)
            val msgs = messages.value!!.apply { add(message) }
            messages.value = msgs
            when (newState) {
                WebSocketState.CREATED -> state.postValue(STATE_CREATED)
                WebSocketState.CONNECTING -> state.postValue(STATE_CONNECTING)
                WebSocketState.OPEN -> state.postValue(STATE_OPEN)
                WebSocketState.CLOSING -> state.postValue(STATE_CLOSING)
                WebSocketState.CLOSED -> state.postValue(STATE_CLOSED)
            }
        },
        handleCallbackErrorMain = { ws, t ->
            val message = newMessage("Server", "Error: ${t.message}")
            val msgs = messages.value!!.apply { add(message) }
            messages.value = msgs
        }
    )

    val systemMessage = SingleLiveEvent<String>()
    val state = MutableLiveData<Int>()
    val inputText = MutableLiveData<String>()
    val messages = MutableLiveData<ArrayList<Message>>()
    val address = Settings.asLiveData(Settings::address)

    init {
        state.value = STATE_CLOSED
        messages.value = arrayListOf()
    }

    private fun newMessage(author: String, text: String) = Message(
        UUID.randomUUID().toString(),
        author,
        Date().getCurrentTime("HH:mm:ss"),
        text
    )

    fun setAddress(text: String) {
        Settings.address = text
    }

    fun sendMessage() {
        val message = newMessage("You", inputText.value!!)
        messages.value?.add(message)
        webSocket?.sendText(message.text)
    }

    fun actionButtonClicked() {
        try {
            if (state.value != STATE_OPEN) {
                webSocket = WebSocketFactory().createSocket(address.value)
                webSocket?.removeListener(webSocketListener)
                webSocket?.addListener(webSocketListener)
                webSocket?.connectAsynchronously()
            } else {
                webSocket?.disconnect()
            }
        } catch (e: Exception) {
            systemMessage.value = e.message.toString()
        }
    }
}
