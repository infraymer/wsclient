package home.infraymer.wsclient.system

import android.os.Handler
import android.os.Looper
import com.neovisionaries.ws.client.WebSocket
import com.neovisionaries.ws.client.WebSocketAdapter
import com.neovisionaries.ws.client.WebSocketState

class CustomWebSocketListener(
    val onStateChangedMain: ((ws: WebSocket, newState: WebSocketState) -> Unit)? = null,
    val handleCallbackErrorMain: ((ws: WebSocket, t: Throwable) -> Unit)? = null
) : WebSocketAdapter() {

    private val handler = Handler(Looper.getMainLooper())

    override fun onStateChanged(websocket: WebSocket, newState: WebSocketState) {
        handler.post { onStateChangedMain?.invoke(websocket, newState) }
    }

    override fun handleCallbackError(websocket: WebSocket, cause: Throwable) {
        handler.post { handleCallbackErrorMain?.invoke(websocket, cause) }
    }
}