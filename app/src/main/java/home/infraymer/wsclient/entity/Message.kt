package home.infraymer.wsclient.entity

data class Message(
    val id: String,
    val author: String,
    val time: String,
    val text: String
)