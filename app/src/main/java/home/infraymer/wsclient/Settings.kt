package home.infraymer.wsclient

import com.chibatching.kotpref.KotprefModel

object Settings : KotprefModel() {

    var address: String by stringPref("")
}