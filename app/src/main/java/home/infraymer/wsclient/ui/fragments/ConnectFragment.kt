package home.infraymer.wsclient.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import home.infraymer.wsclient.MainViewModel
import home.infraymer.wsclient.R
import kotlinx.android.synthetic.main.connect_fragment.*

class ConnectFragment : BaseFragment() {

    companion object {
        fun newInstance() = ConnectFragment()
    }

    override val layoutRes = R.layout.connect_fragment

    private lateinit var viewModel: MainViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(parentFragment!!).get(MainViewModel::class.java)

        addressEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        actionButton.setOnClickListener {
            viewModel.setAddress(addressEditText.text.toString())
            viewModel.actionButtonClicked()
        }
        val addressObserver: Observer<String> = Observer {
            addressEditText.setText(it)
            addressEditText.setSelection(it?.length!!)
        }
        viewModel.address.observe(this, addressObserver)
        observeState()
    }

    private fun observeState() {
        viewModel.state.observe(this, Observer {
            when (it) {
                MainViewModel.STATE_CREATED -> onStateCreated()
                MainViewModel.STATE_OPEN -> onStateOpen()
                MainViewModel.STATE_CONNECTING -> onStateConnecting()
                MainViewModel.STATE_CLOSING -> onStateClosing()
                MainViewModel.STATE_CLOSED -> onStateClosed()
            }
        })
    }

    private fun onStateCreated() {
        statusTextView.text = "Created"
    }

    private fun onStateOpen() {
        actionButton.text = getString(R.string.disconnect)
        actionButton.isEnabled = true
        statusTextView.text = "Open"
    }

    private fun onStateConnecting() {
        actionButton.isEnabled = false
        statusTextView.text = "Connecting"
    }

    private fun onStateClosing() {
        actionButton.isEnabled = false
        statusTextView.text = "Closing"
    }

    private fun onStateClosed() {
        actionButton.text = getString(R.string.connect)
        actionButton.isEnabled = true
        statusTextView.text = "Closed"
    }


}
