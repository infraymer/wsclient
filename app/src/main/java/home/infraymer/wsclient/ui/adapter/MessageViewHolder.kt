package home.infraymer.wsclient.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import home.infraymer.wsclient.entity.Message
import home.infraymer.wsclient.R

class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val author: TextView = itemView.findViewById(R.id.authorTextView)
    private val time: TextView = itemView.findViewById(R.id.timeTextView)
    private val message: TextView = itemView.findViewById(R.id.messageTextView)

    fun bind(item: Message) {
        author.text = item.author
        time.text = item.time
        message.text = item.text
    }
}