package home.infraymer.wsclient.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.FragmentPagerAdapter
import home.infraymer.wsclient.MainViewModel
import home.infraymer.wsclient.R
import home.infraymer.wsclient.snack
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : BaseFragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    override val layoutRes = R.layout.main_fragment

    private lateinit var viewModel: MainViewModel
    private val pagerAdapter: MainPagerAdapter by lazy { MainPagerAdapter() }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.systemMessage.observe(this, Observer { view?.snack(it) })

        viewPager.adapter = pagerAdapter
        tabLayout.setupWithViewPager(viewPager)
    }

    private inner class MainPagerAdapter : FragmentPagerAdapter(childFragmentManager) {
        override fun getItem(position: Int) = when (position) {
            0 -> ConnectFragment.newInstance()
            1 -> MessageFragment.newInstance()
            else -> null
        }

        override fun getCount() = 2

        override fun getPageTitle(position: Int) = when (position) {
            0 -> "Connect"
            1 -> "Messages"
            else -> null
        }
    }
}
