package home.infraymer.wsclient.ui.adapter

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import home.infraymer.wsclient.entity.Message
import home.infraymer.wsclient.R
import home.infraymer.wsclient.inflate

class MessageAdapter : RecyclerView.Adapter<MessageViewHolder>() {

    var data = arrayListOf<Message>()
        set(value) {
            val diffCallback = MessageDiffUtilCallback(field, value)
            val diffResult = DiffUtil.calculateDiff(diffCallback)
            field = value
            diffResult.dispatchUpdatesTo(this)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MessageViewHolder(parent.inflate(R.layout.message_item))

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) = holder.bind(data[position])

    fun add(message: Message) {
        data.add(message)
        notifyItemInserted(data.size - 1)
    }
}