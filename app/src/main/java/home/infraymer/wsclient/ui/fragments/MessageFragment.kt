package home.infraymer.wsclient.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import home.infraymer.wsclient.MainViewModel
import home.infraymer.wsclient.R
import home.infraymer.wsclient.ui.adapter.MessageAdapter
import kotlinx.android.synthetic.main.message_fragment.*

class MessageFragment : BaseFragment() {

    companion object {
        fun newInstance() = MessageFragment()
    }

    override val layoutRes = R.layout.message_fragment

    private lateinit var viewModel: MainViewModel

    private val messageAdapter by lazy { MessageAdapter() }
    private val linearLayoutManager by lazy {
        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false).apply {
            //            reverseLayout = true
            stackFromEnd = true
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(parentFragment!!).get(MainViewModel::class.java)
        with(recyclerView) {
            layoutManager = linearLayoutManager
            adapter = messageAdapter
        }
        inputEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                viewModel.inputText.value = s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        sendButton.setOnClickListener {
            viewModel.sendMessage()
            inputEditText.setText("")
        }
        viewModel.messages.observe(this, Observer { messageAdapter.data = it!! })
    }

}
