package home.infraymer.wsclient

import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.text.SimpleDateFormat
import java.util.*

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun Date.getCurrentTime(format: String): String {
    val df = SimpleDateFormat(format, Locale.getDefault())
    return df.format(this)
}

fun View.visible(show: Boolean = true) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun View.snack(text: String?) {
    if (text != null) Snackbar.make(this, text, Snackbar.LENGTH_LONG)
}